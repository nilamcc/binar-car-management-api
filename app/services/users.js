const userRepositories = require("../repositories/userS");

module.exports = {
    create(requestBody) {
        return userRepositories.create(requestBody);
    },

    update(id, requestBody) {
        return userRepositories.update(id, requestBody);
    },

    delete(id) {
        return userRepositories.delete(id);
    },

    async getUsers() {
        try {
            const users = await userRepositories.findAll();
            return {
                data: users,
            };
        } catch (err) {
            throw err;
        }
    },

    findByPk(id) {
        return userRepositories.findByPk(id)
    },

    findOne(id) {
        return userRepositories.findOne(id)
    },

    get(id) {
        return userRepositories.find(id);
    },
};
