const userService = require("../../../services/users");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../../../models");
const SALT = 10;

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, SALT, (err, encryptedPassword) => {
            if (!!err) {
                reject(err);
                return;
            }
            resolve(encryptedPassword);
        });
    });
}

function checkPassword(encryptedPassword, password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
            if (!!err) {
                reject(err);
                return;
            }
            resolve(isPasswordCorrect);
        });
    });
}

function createToken(payload) {
    return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia", {
        expiresIn: "1d"
    });
}

module.exports = {
    async register(req, res) {
        const { username, email, role } = req.body;
        const password = await encryptPassword(req.body.password);
        const user = await userService.create({ username, email, password, role })
            .then((user) => {
                res.status(201).json({
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    role: user.role,
                    createdAt: user.createdAt,
                    updatedAt: user.updatedAt,
                });
            })
            .catch((err) => {
                res.status(409).json({
                    status: "Failed",
                    message: err.message,
                });
            });
    },

    async login(req, res) {
        const email = req.body.email.toLowerCase(); // Biar case insensitive
        const password = req.body.password;
        const username = req.body.username;
        const user = await userService.findOne({
            where: { email },
        });

        if (!user) {
            res.status(404).json({ message: "Email not found" });
            return;
        }

        const isPasswordCorrect = await checkPassword(user.password, password);

        if (!isPasswordCorrect) {
            res.status(401).json({ message: "The password you entered is wrong" });
            return;
        }

        const token = createToken({
            id: user.id,
            username: user.username,
            email: user.email,
            role: user.role,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        });

        res.status(201).json({
            id: user.id,
            username: user.username,
            email: user.email,
            token,
            role: user.role,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        });
    },

    async whoAmI(req, res) {
        res.status(200).json(req.user);
    },

    async authorize(req, res, next) {
        try {
            const bearerToken = req.headers.authorization
            const token = bearerToken.split("Bearer ")[1]
            const tokenPayload = jwt.verify(
                token,
                process.env.JWT_SIGNATURE_KEY || "Rahasia"
            );
            req.user = await userService.findByPk(tokenPayload.id);
            next();
        } catch (err) {
            console.error(err);
            res.status(401).json({
                message: "Unauthorized",
            });
        }
    },

    async isAdminOrSuperAdmin(req, res, next) {
        if (!(req.user.role === "admin" || req.user.role === "superadmin")) {
            res.json({
                message: "You are not Admin or Super Admin",
            });
            return;
        }
        next();
    },

    async isSuperAdmin(req, res, next) {
        if (!(req.user.role === "superadmin")) {
            res.json({
                message: "You're not a Super Admin, can't access this feature",
            });
            return;
        }
        next();
    },

    list(req, res) {
        userService
            .list()
            .then(({ data, count }) => {
                res.status(200).json({
                    status: "OK",
                    data: { users: data },
                    meta: { total: count },
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    create(req, res) {
        userService
            .create(req.body)
            .then((user) => {
                res.status(201).json({
                    status: "OK",
                    data: user,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "Failed",
                    message: err.message,
                });
            });
    },
    update(req, res) {
        req.body.role = "admin";
        userService.update(req.params.id, req.body)
            .then((user) => {
                res.status(200).json({
                    status: "OK",
                    message: "Change member to Admin Succesfully"
                })
            })
            .catch((err) => {
                res.status(422).json({
                    status: "Failed",
                    message: err.message,
                });
            });
    },

    show(req, res) {
        userService
            .getUsers()
            .then((user) => {
                res.status(201).json({
                    status: "OK",
                    data: {
                        user
                    }
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "Failed",
                    message: err.message,
                });
            });
    },

    destroy(req, res) {
        const user = userService.delete(req.params.id)
            .then(() => {
                res.status(201).json({
                    status: "Data deleted succesfully",
                })
            })
            .catch((err) => {
                res.status(400).json({
                    status: "Failed",
                    errors: [err.message]
                });
            });
    },

    getUsers(req, res) {
        const user = userService.findByPk(req.params.id)
            .then((user) => {
                res.status(201).json({
                    status: "OK",
                    data: {
                        user
                    }
                })
            })
            .catch((err) => {
                res.status(400).json({
                    status: "Failed",
                    errors: [err.message]
                });
            });
    }
};

