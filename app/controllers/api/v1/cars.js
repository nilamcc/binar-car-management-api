const carsService = require("../../../services/cars")

module.exports = {
    list(req, res) {
        const cars = carsService.list({
            where: { isDeleted: false }
        })
            .then((cars, count) => {
                res.status(200).json({
                    status: "Success",
                    data: {
                        cars
                    },
                    meta: { total: count },
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "Failed",
                    errors: [err.message]
                })
            })
    },

    create(req, res) {
        req.body.createdBy = req.user.username;
        const cars = carsService.create(req.body)
            .then((cars) => {
                res.status(201).json({
                    status: "data created successfully",
                    data: {
                        cars
                    }
                })
            })
            .catch((err) => {
                res.status(400).json({
                    status: "Failed",
                    errors: [err.message]
                })
            })
    },

    update(req, res) {
        req.body.createdBy = req.user.username;
        req.body.updatedBy = req.user.username;
        const cars = carsService.update(req.params.id, req.body)
            .then((cars) => {
                res.status(201).json({
                    status: "data updated successfully",
                })
            })
            .catch((err) => {
                res.status(400).json({
                    status: "Failed",
                    errors: [err.message]
                })
            })
    },

    show(req, res) {
        const cars = carsService.get(req.params.id)
            .then((cars) => {
                res.status(201).json({
                    status: "OK",
                    data: cars,
                })
            })
            .catch((err) => {
                res.status(400).json({
                    status: "Failed",
                    errors: [err.message]
                });
            });
    },

    destroy(req, res) {
        const cars = carsService.delete(req.params.id, { isDeleted: true, deletedBy: req.user.username })
            .then(() => {
                res.status(201).json({
                    status: "Data deleted succesfully",
                    deletedBy: req.user.username
                })
            })
            .catch((err) => {
                res.status(400).json({
                    status: "Failed",
                    errors: [err.message]
                });
            });
    },

}
