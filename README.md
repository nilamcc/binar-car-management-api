# Binar: Car Management API

Repository ini merupakan tugas dari challenge chapter 6. Pada repository ini terdapat endpoint untuk register dan login untuk user, endpoint untuk feature khusus superadmin yaitu bisa membuat user biasa menjadi admin, endpoint untuk melakukan CRUD terhadap data mobil yang hanya dapat dilakukan oleh admin dan super admin, endpoint untuk melihat current user berdasarkan token (whoami). 

## Getting Started

untuk menggunakan repository ini dapat dimulai dengan melakukan pengubahan terhadap akun postgres yang kalian miliki yang berada pada `config/database.js`

Lalu untuk menjalankan development server, kalian tinggal jalanin salah satu script di package.json, yang namanya `start`.

```sh
npm install
npx sequelze db:create
npx sequelize db:migrate
npm run start
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `npm db:create` digunakan untuk membuat database
- `npm db:drop` digunakan untuk menghapus database
- `npm db:migrate` digunakan untuk menjalankan database migration
- `npm db:seed` digunakan untuk melakukan seeding
- `npm db:rollback` digunakan untuk membatalkan migrasi terakhir




